<?php

namespace Drupal\smugmug_external_entities\Plugin\ExternalEntities\StorageClient;

use Drupal\external_entities\ExternalEntityInterface;
use Drupal\external_entities\Plugin\PluginFormTrait;
use Drupal\external_entities\StorageClient\ExternalEntityStorageClientBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\smugmug_api\Service\Image as ImageService;
use Drupal\smugmug_api\Service\Album as AlbumService;

/**
 * External entities storage client based on a REST API.
 *
 * @ExternalEntityStorageClient(
 *   id = "smugmug_image",
 *   label = @Translation("Smugmug image"),
 *   description = @Translation("Retrieves external entities from the WA REST API."),
 * )
 */
class Image extends ExternalEntityStorageClientBase {

  use PluginFormTrait;
  /**
   * @var \Drupal\smugmug_api\Service\Image
   */
  protected $image;

  /**
   * @var \Drupal\smugmug_api\Service\Album
   */
  protected $albums;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setImageService($container->get('smugmug_api.image'));
    $instance->setAlbumService($container->get('smugmug_api.album'));
    return $instance;
  }


  /**
   * Set the service that allow the plugin obtain album images.
   *
   * @param \Drupal\smugmug_api\Service\Image $image_service
   *   Album service.
   */
  public function setImageService(ImageService $image_service) {
    $this->image = $image_service;
  }

  /**
   * Set the service that allow the plugin obtain album images.
   *
   * @param \Drupal\smugmug_api\Service\Album $album
   *   Album service.
   */
  public function setAlbumService(AlbumService $album) {
    $this->albums = $album;
  }

  /**
   * {@inheritdoc}
   */
  public function save(ExternalEntityInterface $entity) {
    throw new \Exception('Save is not supported');
  }

  /**
   * {@inheritdoc}
   */
  public function delete(ExternalEntityInterface $entity) {
    throw new \Exception('Delete is not supported');
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $images = $this->image->getImage(implode(',', $ids));
    return isset($images['ImageKey']) ? [$images] : $images;
  }

  /**
   * {@inheritdoc}
   */
  public function query(array $parameters = [], array $sorts = [], $start = NULL, $length = NULL) {

    foreach ($parameters as $parameter) {
      switch ($parameter['field']) {

        case 'album':
          $album = reset($parameter['value']);
          return $this->albums->getAlbumImages($album);

      }
    }

  }

}
