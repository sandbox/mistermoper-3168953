<?php

namespace Drupal\smugmug_external_entities\Plugin\ExternalEntities\StorageClient;

use Drupal\external_entities\ExternalEntityInterface;
use Drupal\external_entities\Plugin\PluginFormTrait;
use Drupal\external_entities\StorageClient\ExternalEntityStorageClientBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\smugmug_api\Service\Album as AlbumService;

/**
 * External entities storage client based on a REST API.
 *
 * @ExternalEntityStorageClient(
 *   id = "smugmug_album",
 *   label = @Translation("Smugmug album"),
 *   description = @Translation("Retrieves external entities from the WA REST API."),
 * )
 */
class Album extends ExternalEntityStorageClientBase {

  use PluginFormTrait;
  /**
   * @var \Drupal\smugmug_api\Service\Album
   */
  protected $albums;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
  {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->setAlbumService($container->get('smugmug_api.album'));
    return $instance;
  }

  /**
   * Set the service that allow the plugin obtain the albums.
   *
   * @param \Drupal\smugmug_api\Service\Album $albums
   *   Album list.
   */
  public function setAlbumService(AlbumService $albums) {
    $this->albums = $albums;
  }

  /**
   * {@inheritdoc}
   */
  public function save(ExternalEntityInterface $entity) {
    throw new \Exception('Save is not supported');
  }

  /**
   * {@inheritdoc}
   */
  public function delete(ExternalEntityInterface $entity) {
    throw new \Exception('Delete is not supported');
  }

  /**
   * {@inheritdoc}
   */
  public function loadMultiple(array $ids = NULL) {
    $albums = $this->albums->getAlbum(implode(',', $ids));
    return isset($albums['AlbumKey']) ? [$albums] : $albums;
  }

  /**
   * {@inheritdoc}
   */
  public function query(array $parameters = [], array $sorts = [], $start = NULL, $length = NULL) {

    foreach ($parameters as $parameter) {
      switch ($parameter['field']) {

        case 'user':
          $user = reset($parameter['user']);
          return $this->albums->getAlbumsByUser($user);

      }
    }
  }

}
